# 2.1.0 (2021-04-27)

## Features
* add transpose command (8dae92b)
* add rotate command (70343c8)

## Refactoring
* move pre-command log message to after the command (c7ac2b6)
* add log message stating the command being performed (22bc129)

# 2.0.0 (2021-04-26)

## Features
* add convert command functionality and tests (d1355bc)
* implement quality argument and skip-on-conflict functionality (c3b2397)
* allow 'negative' crops (6eca123)
* add short arguments for width and height (0598f0d)
* add multiple input paths implementation to main (cb5acbb)
* add ability to specify multiple input paths (e85d52d)

## Fixes
* catch cases where crop boundaries are inverted (ed7f29d)
* patch resize ratio, format conversion, and quality bugs (1f01640)
* add missing f-string designations to log messages (51c09bf)

## Refactoring
* reword command descriptions (9d328d2)
* rename test file (6018d83)
* finish main rewrite (9989560)
* change formatting slightly (30b94ee)
* clarify output and resize error messages (18d1b04)
* begin main rewrite (06df2c4)
* change how error-checking is handled (bba3be8)
* shorten log formatter (dee2036)
* add a list constant for the format option (a4c88e1)
* change log handling and add format option (7603994)
* rewrite commands and tests for commands (f16acc7)
* remove log message and fix two typos (bf5b7fd)
* rework how argument parsing and checking is handled (affe316)
* change how option checking is performed (2b5e5b4)
* change num. args check to reflect new crop allowance (f76b0d5)
* change how output paths are validated (ff3180e)
* rework how exceptions are raised and handled (215eb35)
* rewrite command-line argparse help (2c0e4e2)

# 1.0.0 (2021-04-09)
