"""Unit tests for pyimgutil package's commands.py.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from pathlib import Path

from PIL import Image, ImageDraw
import pytest

from pyimgutil import commands


class TestCropImage:
    @pytest.fixture
    def simple_im(self):
        return Image.new("RGBA", (400, 600), "#999999")

    @pytest.fixture
    def quad_im(self):
        # Quadrant: 1=green, 2=red, 3=blue, 4=yellow.
        im = Image.new("RGBA", (800, 800), "#000000")
        imd = ImageDraw.ImageDraw(im)
        imd.rectangle((400, 0, 800, 400), fill="#00ff00")
        imd.rectangle((0, 0, 400, 400), fill="#ff0000")
        imd.rectangle((0, 400, 400, 800), fill="#0000ff")
        imd.rectangle((400, 400, 800, 800), fill="#ffff00")
        return im

    @pytest.mark.parametrize(
        "lb, rb, ub, bb",
        [
            (400, None, None, None),
            (None, 0, None, None),
            (None, None, 600, None),
            (None, None, None, 0),
        ],
    )
    def test_crop_errors(self, simple_im, lb, rb, ub, bb):
        with pytest.raises(ValueError):
            commands.crop_image(simple_im, lb, rb, ub, bb)

    @pytest.mark.parametrize(
        "lb, rb, ub, bb, width, height",
        [
            (None, None, None, None, 400, 600),
            (100, None, None, None, 300, 600),
            (None, 100, None, None, 100, 600),
            (None, None, 100, None, 400, 500),
            (None, None, None, 100, 400, 100),
            (100, 200, 200, 400, 100, 200),
            (-100, None, None, None, 500, 600),
            (None, 500, None, None, 500, 600),
            (None, None, -100, None, 400, 700),
            (None, None, None, 700, 400, 700),
            (-100, 500, -200, 800, 600, 1000),
        ],
    )
    def test_simple_crop(self, simple_im, lb, rb, ub, bb, width, height):
        cropped_im = commands.crop_image(simple_im, lb, rb, ub, bb)
        assert cropped_im.width == width
        assert cropped_im.height == height

    @pytest.mark.parametrize(
        "lb, rb, ub, bb, width, height, colors",
        [
            (None, None, None, None, 800, 800, 4),
            # Square of green, red, blue, yellow.
            (200, 600, 200, 600, 400, 400, 4),
            # Leave only green.
            (500, None, None, 300, 300, 300, 1),
            # Leave only red.
            (None, 300, None, 300, 300, 300, 1),
            # Leave only blue.
            (None, 300, 500, None, 300, 300, 1),
            # Leave only yellow.
            (500, None, 500, None, 300, 300, 1),
        ],
    )
    def test_quad_crop(self, quad_im, lb, rb, ub, bb, width, height, colors):
        cropped_im = commands.crop_image(quad_im, lb, rb, ub, bb)
        assert cropped_im.width == width
        assert cropped_im.height == height
        assert len(cropped_im.getcolors()) == colors


class TestGetOutputDir:
    def test_output_none(self):
        output_path = commands.get_output_dir(None)
        assert output_path.exists() is True
        assert output_path == Path.cwd()

    def test_output_not_none(self, tmp_path):
        output_path = commands.get_output_dir(tmp_path)
        assert output_path.exists() is True
        assert output_path == tmp_path


class TestGetValidFiles:
    @pytest.fixture
    def test_files(self, tmp_path):
        (tmp_path / "dir").mkdir()
        for ext in commands.SEARCH_EXTS:
            (tmp_path / ("file" + ext)).touch()
            (tmp_path / "dir" / ("file" + ext)).touch()
        (tmp_path / "file.txt").touch()
        (tmp_path / "dir" / "file.txt").touch()

    def test_error(self, tmp_path):
        path = (tmp_path / "nonexistent")
        with pytest.raises(ValueError):
            commands.get_valid_files(path, False)

    @pytest.mark.parametrize(
        "file, length",
        [
            ("file.bmp", 1),
            ("file.gif", 1),
            ("file.ico", 1),
            ("file.jpeg", 1),
            ("file.jpg", 1),
            ("file.png", 1),
            ("file.ppm", 1),
            ("file.JpEg", 1),
            ("not_jpg.txt", 0),
            ("not_png.TxT", 0),
        ],
    )
    def test_file(self, tmp_path, file, length):
        path = (tmp_path / file)
        path.touch()
        valid_files = commands.get_valid_files(path, False)
        assert len(valid_files) == length
        if len(valid_files) == 1:
            assert valid_files[0] == (tmp_path / file)

    def test_dir_nonrecursive(self, tmp_path, test_files):
        valid_files = commands.get_valid_files(tmp_path, False)
        assert len(valid_files) == len(commands.SEARCH_EXTS)
        for ext in commands.SEARCH_EXTS:
            assert (tmp_path / ("file" + ext)) in valid_files
        assert (tmp_path / "file.txt") not in valid_files
        assert (tmp_path / "dir" / "file.txt") not in valid_files

    def test_dir_recursive(self, tmp_path, test_files):
        valid_files = commands.get_valid_files(tmp_path, True)
        assert len(valid_files) == len(commands.SEARCH_EXTS) * 2
        for ext in commands.SEARCH_EXTS:
            assert (tmp_path / ("file" + ext)) in valid_files
            assert (tmp_path / "dir" / ("file" + ext)) in valid_files
        assert (tmp_path / "file.txt") not in valid_files
        assert (tmp_path / "dir" / "file.txt") not in valid_files


class TestModExt:
    @pytest.mark.parametrize(
        "old_name, ext, new_name",
        [
            (Path("file.bmp"), "gif", Path("file.gif")),
            (Path("file.ico"), "jpeg", Path("file.jpg")),
            (Path("file.jpg"), "PNG", Path("file.png")),
        ],
    )
    def test(self, tmp_path, old_name, ext, new_name):
        old_path = tmp_path / old_name
        new_path = tmp_path / new_name
        assert commands.mod_ext(old_path, ext) == new_path


class TestModName:
    @pytest.mark.parametrize(
        "old_name, text, prepend, new_name",
        [
            (Path("file.jpg"), "text_", True, Path("text_file.jpg")),
            (Path("file.jpg"), "_text", False, Path("file_text.jpg")),
        ],
    )
    def test(self, tmp_path, old_name, text, prepend, new_name):
        old_path = tmp_path / old_name
        new_path = tmp_path / new_name
        assert commands.mod_name(old_path, text, prepend) == new_path


class TestModParent:
    def test(self, tmp_path):
        name = "file.bmp"
        old_path = tmp_path / name
        new_parent = tmp_path / "dir"
        new_path = new_parent / "file.bmp"
        assert commands.mod_parent(old_path, new_parent) == new_path


class TestResizeImage:
    @pytest.fixture
    def im(self):
        return Image.new("RGBA", (421, 749), "#999999")

    @pytest.mark.parametrize(
        "scale",
        [
            (0.74),
            (1),
            (2.481239),
        ]
    )
    def test_scale(self, im, scale):
        re_im = commands.resize_image(im, None, None, scale, False)
        ratio = im.width / im.height
        assert re_im.width == int(im.width * scale)
        assert re_im.height == int(im.height * scale)
        new_ratio = re_im.width / re_im.height
        assert new_ratio == pytest.approx(ratio, 1e-2)

    @pytest.mark.parametrize(
        "width_arg, height_arg",
        [
            (208, None),
            (846, None),
            (None, 208),
            (None, 846),
        ]
    )
    def test_ratio(self, im, width_arg, height_arg):
        re_im = commands.resize_image(im, width_arg, height_arg, None, True)
        ratio = im.width / im.height
        if width_arg is not None:
            assert re_im.width == width_arg
            assert re_im.height == int(width_arg / ratio)
        if height_arg is not None:
            assert re_im.width == int(ratio * height_arg)
            assert re_im.height == height_arg
        new_ratio = re_im.width / re_im.height
        assert new_ratio == pytest.approx(ratio, 1e-2)

    @pytest.mark.parametrize(
        "width_arg, height_arg",
        [
            (208, 846),
            (846, 208),
            (208, None),
            (846, None),
            (None, 208),
            (None, 846),
            (None, None),
        ]
    )
    def test_width_height(self, im, width_arg, height_arg):
        re_im = commands.resize_image(im, width_arg, height_arg, None, False)
        if width_arg is not None:
            assert re_im.width == width_arg
        else:
            assert re_im.width == im.width
        if height_arg is not None:
            assert re_im.height == height_arg
        else:
            assert re_im.height == im.height


class TestRotateImage:
    @pytest.fixture
    def im(self):
        return Image.new("RGBA", (800, 800), "#0000ff")

    @pytest.fixture
    def quad_im(self):
        # Quadrant: 1=green, 2=red, 3=blue, 4=yellow.
        im = Image.new("RGBA", (800, 800), "#000000")
        imd = ImageDraw.ImageDraw(im)
        imd.rectangle((400, 0, 800, 400), fill="#00ff00")
        imd.rectangle((0, 0, 400, 400), fill="#ff0000")
        imd.rectangle((0, 400, 400, 800), fill="#0000ff")
        imd.rectangle((400, 400, 800, 800), fill="#ffff00")
        return im

    @pytest.mark.parametrize(
        "angle, width, height, color",
        [
            (-90, 800, 800, (255, 0, 0, 255)),
            (-45, 1132, 1132, (0, 0, 0, 0)),
            (None, 800, 800, (0, 255, 0, 255)),
            (45, 1132, 1132, (0, 0, 0, 0)),
            (90, 800, 800, (255, 255, 0, 255)),
        ],
    )
    def test_angle(self, quad_im, angle, width, height, color):
        ro_im = commands.rotate_image(quad_im, angle, False)
        assert ro_im.width == width
        assert ro_im.height == height
        assert ro_im.getpixel((ro_im.width - 1, 0)) == color

    @pytest.mark.parametrize(
        "angle, fill, color",
        [
            (-45, False, (0, 0, 0, 0)),
            (-45, True, (0, 0, 0, 255)),
        ],
    )
    def test_fill(self, im, angle, fill, color):
        ro_im = commands.rotate_image(im, angle, fill)
        assert im.getpixel((0, 0)) == (0, 0, 255, 255)
        assert ro_im.getpixel((0, 0)) == color


class TestSaveImage:
    @pytest.fixture
    def blue_im(self):
        return Image.new("RGBA", (100, 100), "#0000ff")

    @pytest.fixture
    def green_im(self):
        return Image.new("RGBA", (100, 100), "#00ff00")

    @pytest.fixture
    def png_im(self):
        return Image.new("RGBA", (100, 100))

    @pytest.fixture
    def jpg_im(self):
        return Image.new("RGB", (100, 100))

    def test_conflict_error(self, tmp_path, blue_im, green_im):
        path = (tmp_path / "file.png")
        blue_im.save(path)
        assert Image.open(path).getcolors()[0][1] == (0, 0, 255, 255)
        with pytest.raises(FileExistsError):
            commands.save_image(green_im, path, None, False)
        assert Image.open(path).getcolors()[0][1] == (0, 0, 255, 255)

    def test_conflict_no_error(self, tmp_path, blue_im, green_im):
        path = (tmp_path / "file.png")
        blue_im.save(path)
        assert Image.open(path).getcolors()[0][1] == (0, 0, 255, 255)
        try:
            commands.save_image(green_im, path, None, True)
        except BaseException:
            pytest.fail()
        assert Image.open(path).getcolors()[0][1] == (0, 255, 0, 255)

    def test_keep_error(self, tmp_path, caplog, jpg_im):
        path = (tmp_path / "file.jpg")
        commands.save_image(jpg_im, path, None, False)
        assert caplog.records[0].levelno == 30
        assert "not be saved with 'keep' quality" in caplog.records[0].message

    def test_keep_no_error(self, tmp_path, caplog, png_im):
        path = (tmp_path / "file.png")
        commands.save_image(png_im, path, None, False)
        assert caplog.records[0].levelno == 10
        assert "File saved with 'keep' quality" in caplog.records[0].message

    def test_quality(self, tmp_path, jpg_im):
        path = (tmp_path / "file.png")
        try:
            commands.save_image(jpg_im, path, 0, True)
            commands.save_image(jpg_im, path, 100, True)
        except BaseException:
            pytest.fail()

    @pytest.mark.parametrize(
        "mode1, mode2, ext1, ext2",
        [
            ("RGBA", "RGB", "png", "jpg"),
            ("P", "RGB", "gif", "jpg"),
            ("P", "RGB", "gif", "ppm"),
        ],
    )
    def test_jpg_conversion(self, tmp_path, mode1, mode2, ext1, ext2):
        start_path = tmp_path / ("file." + ext1)
        end_path = tmp_path / ("file." + ext2)

        im = Image.new("RGBA", (100, 100))
        im.save(start_path)
        im = Image.open(start_path)
        assert im.mode == mode1

        try:
            commands.save_image(im, end_path, None, False)
        except BaseException:
            pytest.fail()

        im = Image.open(end_path)
        assert im.mode == mode2


class TestTranspose:
    @pytest.fixture
    def split_im(self):
        im = Image.new("RGBA", (800, 800), "#000000")
        imd = ImageDraw.ImageDraw(im)
        imd.rectangle((0, 0, 400, 800), fill="#ff0000")
        imd.rectangle((401, 0, 800, 800), fill="#0000ff")
        return im

    def test(self, split_im):
        assert split_im.getpixel((200, 200)) == (255, 0, 0, 255)
        assert split_im.getpixel((200, 600)) == (255, 0, 0, 255)
        assert split_im.getpixel((600, 200)) == (0, 0, 255, 255)
        assert split_im.getpixel((600, 600)) == (0, 0, 255, 255)
        tr_im = commands.transpose_image(split_im)
        assert tr_im.getpixel((200, 200)) == (0, 0, 255, 255)
        assert tr_im.getpixel((200, 600)) == (0, 0, 255, 255)
        assert tr_im.getpixel((600, 200)) == (255, 0, 0, 255)
        assert tr_im.getpixel((600, 600)) == (255, 0, 0, 255)
