"""Unit tests for pyimgutil package's arguments.py.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import logging
from pathlib import Path
import sys

import pytest

import pyimgutil
from pyimgutil import arguments


class TestCheckConflicts:
    class MockArgs():
        def __init__(self, **kwargs):
            self.command = kwargs.setdefault("command")
            self.width = kwargs.setdefault("width")
            self.height = kwargs.setdefault("height")
            self.ratio = kwargs.setdefault("ratio", False)
            self.scale = kwargs.setdefault("scale")

    @pytest.mark.parametrize(
        "mock_args",
        [
            (MockArgs()),
            (MockArgs(command="resize", ratio=True, width=None, height=None)),
            (MockArgs(command="resize", ratio=True, width=0, height=0)),
            (MockArgs(command="resize", scale=0, width=0)),
            (MockArgs(command="resize", scale=0, height=0)),
        ],
    )
    def test_error(self, mock_args):
        with pytest.raises(ValueError):
            arguments.check_conflicts(mock_args)

    def test_no_error(self):
        mock_args = TestCheckConflicts.MockArgs(
            command="resize",
            width=0,
            height=0,
        )
        try:
            arguments.check_conflicts(mock_args)
        except ValueError:
            pytest.fail()


class TestCheckCrop:
    @pytest.mark.parametrize(
        "left, right, upper, bottom",
        [
            (0, 0, None, None),
            (None, None, 0, 0),
        ],
    )
    def test_error(self, left, right, upper, bottom):
        with pytest.raises(ValueError):
            arguments.check_crop(left, right, upper, bottom)

    @pytest.mark.parametrize(
        "left, right, upper, bottom",
        [
            (0, 1, None, None),
            (None, None, 0, 1),
            (None, None, None, None),
        ],
    )
    def test_no_error(self, left, right, upper, bottom):
        try:
            arguments.check_crop(left, right, upper, bottom)
        except ValueError:
            pytest.fail()


class TestCheckFormat:
    @pytest.mark.parametrize(
        "ext",
        [
            ("jpg"),
            ("ext"),
            (".png"),
        ],
    )
    def test_error(self, ext):
        with pytest.raises(ValueError):
            arguments.check_format(ext)

    @pytest.mark.parametrize(
        "ext",
        [
            ("bmp"),
            ("gif"),
            ("ico"),
            ("jpeg"),
            ("png"),
            ("ppm"),
            ("PPM"),
        ],
    )
    def test_no_error(self, ext):
        try:
            arguments.check_format(ext)
        except ValueError:
            pytest.fail()


class TestCheckOutput:
    def test_file(self, tmp_path):
        path = Path(tmp_path / "file.jpg")
        path.touch()
        with pytest.raises(ValueError):
            arguments.check_output(path)

    def test_nonexistent_parent_dir(self, tmp_path):
        path = Path(tmp_path / "nonexistent_dir" / "nonexistent_dir")
        with pytest.raises(ValueError):
            arguments.check_output(path)

    def test_dir(self, tmp_path):
        path = Path(tmp_path)
        try:
            arguments.check_output(path)
        except ValueError:
            pytest.fail()

    def test_parent_dir(self, tmp_path):
        path = Path(tmp_path / "nonexistent_dir")
        try:
            arguments.check_output(path)
        except ValueError:
            pytest.fail()


class TestCheckQuality:
    @pytest.mark.parametrize(
        "quality",
        [
            (-1),
            (101),
        ],
    )
    def test_error(self, quality):
        with pytest.raises(ValueError):
            arguments.check_quality(quality)

    @pytest.mark.parametrize(
        "quality",
        [
            (0),
            (100),
        ],
    )
    def test_no_error(self, quality):
        try:
            arguments.check_quality(quality)
        except ValueError:
            pytest.fail()


class TestCheckResize:
    @pytest.mark.parametrize(
        "width, height, scale",
        [
            (0, None, None),
            (None, 0, None),
            (None, None, 0),
        ],
    )
    def test_error(self, width, height, scale):
        with pytest.raises(ValueError):
            arguments.check_resize(width, height, scale)

    @pytest.mark.parametrize(
        "width, height, scale",
        [
            (1, None, None),
            (None, 1, None),
            (None, None, 1),
        ],
    )
    def test_no_error(self, width, height, scale):
        try:
            arguments.check_resize(width, height, scale)
        except ValueError:
            pytest.fail()


class TestConfigureLogging:
    class MockLogger:
        def __init__(self, level=0, handler=None):
            self.level = level
            self.handlers = []
            if handler is not None:
                self.handlers.append(handler)

        def setLevel(self, new_level):
            self.level = new_level

        def addHandler(self, new_handler):
            self.handlers.append(new_handler)

        def removeHandler(self, del_handler):
            index = self.handlers.index(del_handler)
            del self.handlers[index]

    @pytest.fixture
    def mock_logger(self):
        return TestConfigureLogging.MockLogger()

    @pytest.fixture
    def mock_null_handler(self):
        return logging.NullHandler()

    @pytest.fixture
    def mock_stream_handler(self):
        return logging.StreamHandler()

    @pytest.fixture
    def logger_patch(
        self,
        monkeypatch,
        mock_logger,
        mock_null_handler,
        mock_stream_handler,
    ):
        # Patch logging because tests can change the log level.
        monkeypatch.setattr(
            pyimgutil.logger, "setLevel", mock_logger.setLevel
        )
        monkeypatch.setattr(
            pyimgutil.logger, "addHandler", mock_logger.addHandler
        )
        monkeypatch.setattr(
            pyimgutil.logger, "removeHandler", mock_logger.removeHandler
        )
        monkeypatch.setattr(
            pyimgutil, "null_handler", mock_null_handler
        )
        monkeypatch.setattr(
            pyimgutil, "stream_handler", mock_stream_handler
        )

    @pytest.mark.parametrize(
        "verbose, level",
        [
            (0, 0),
            (1, 20),
            (2, 10),
            (3, 10),
        ],
    )
    def test(
        self,
        mock_logger,
        mock_null_handler,
        mock_stream_handler,
        logger_patch,
        verbose,
        level,
    ):
        assert mock_logger.level == 0
        assert mock_logger.handlers == []
        arguments.configure_logging(verbose)
        assert mock_logger.level == level
        if verbose == 0:
            assert mock_logger.handlers == [mock_null_handler]
        else:
            assert mock_logger.handlers == [mock_stream_handler]


class TestParseArgs:
    def test_no_args(self, monkeypatch):
        monkeypatch.setattr(sys, "argv", ["prog"])
        parsed_args = arguments.parse_args()
        assert parsed_args.command is None

    @pytest.mark.parametrize(
        "arg, result",
        [
            ("output", None),
            ("prepend", None),
            ("append", None),
            ("quality", None),
            ("format", None),
            ("recursive", False),
            ("force", False),
            ("verbose", 0),
        ],
    )
    def test_global_defaults(self, arg, result):
        parsed_args = arguments.parse_args(["crop", "input"])
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "args, arg, result",
        [
            ([], "input",  [Path("input")]),
            (["1", "2"], "input", [Path("input"), Path("1"), Path("2")]),
            (["-o", "output"], "output", Path("output")),
            (["-p", "prepend"], "prepend", "prepend"),
            (["-a", "append"], "append", "append"),
            (["--quality", "0"], "quality", 0),
            (["--format", "png"], "format", "png"),
            (["--recursive"], "recursive", True),
            (["--force"], "force", True),
            (["-v"], "verbose", 1),
            (["-vvvv"], "verbose", 4),
        ],
    )
    def test_global_args(self, args, arg, result):
        test_args = ["crop", "input"]
        test_args.extend(args)
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "arg, result",
        [
            ("left", None),
            ("right", None),
            ("upper", None),
            ("bottom", None),
        ],
    )
    def test_crop_defaults(self, arg, result):
        parsed_args = arguments.parse_args(["crop", "input"])
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "args, arg, result",
        [
            (["-l", "0"], "left", 0),
            (["-r", "0"], "right", 0),
            (["-u", "0"], "upper", 0),
            (["-b", "0"], "bottom", 0),
        ],
    )
    def test_crop_args(self, args, arg, result):
        test_args = ["crop", "input"]
        test_args.extend(args)
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "arg, result",
        [
            ("width", None),
            ("height", None),
            ("scale", None),
            ("ratio", False),
        ],
    )
    def test_resize_defaults(self, arg, result):
        parsed_args = arguments.parse_args(["resize", "input"])
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "args, arg, result",
        [
            (["-W", "0"], "width", 0),
            (["-H", "0"], "height", 0),
            (["-s", "0"], "scale", 0),
            (["-r"], "ratio", True),
        ],
    )
    def test_resize_args(self, args, arg, result):
        test_args = ["resize", "input"]
        test_args.extend(args)
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "args, arg, result",
        [
            (["--format", "JPEG"], "format", "JPEG"),
        ],
    )
    def test_convert(self, args, arg, result):
        test_args = ["convert", "input"]
        test_args.extend(args)
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, type(None)):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "arg, result",
        [
            ("angle", None),
            ("fill", False),
        ],
    )
    def test_rotate_default(self, arg, result):
        test_args = ["rotate", "input"]
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    @pytest.mark.parametrize(
        "args, arg, result",
        [
            (["-A", "-2473"], "angle", -2473),
            (["-A", "0"], "angle", 0),
            (["-A", "818"], "angle", 818),
            (["--fill"], "fill", True),
        ],
    )
    def test_rotate_args(self, args, arg, result):
        test_args = ["rotate", "input"]
        test_args.extend(args)
        parsed_args = arguments.parse_args(test_args)
        if isinstance(result, (bool, type(None))):
            assert getattr(parsed_args, arg) is result
        else:
            assert getattr(parsed_args, arg) == result

    def test_transpose(self):
        test_args = ["transpose", "input"]
        try:
            arguments.parse_args(test_args)
        except BaseException:
            pytest.fail()

    @pytest.mark.parametrize(
        "args",
        [
            # Incorrect positional args.
            (["command"]),
            # Missing positional arg following a command.
            (["crop"]),
            # Incorrect optional argument.
            (["crop", "input", "-z"]),
            # Missing an optional arg's value.
            (["crop", "input", "--quality"]),
        ],
    )
    def test_error(self, args):
        with pytest.raises(SystemExit):
            arguments.parse_args(args)


class TestParsePreArgs:
    @pytest.mark.parametrize(
        "args, verbose",
        [
            ([], 0),
            (["-v"], 1),
            (["-vv"], 2),
            (["-vvv"], 3),
        ],
    )
    def test(self, args, verbose):
        parsed_args = arguments.parse_pre_args(args)
        assert parsed_args.verbose == verbose
