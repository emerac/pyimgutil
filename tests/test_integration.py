"""Integration tests for the pyimgutil package.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import collections

from PIL import Image
import pytest

from pyimgutil.__main__ import main


class TestErrors:
    @pytest.mark.parametrize(
        "args",
        [
            ([]),
            (["resize", "path", "-r"]),
            (["resize", "path", "-r", "-W", "0", "-H", "0"]),
            (["resize", "path", "-s", "0", "-W", "0"]),
        ],
    )
    def test_arg_errors(self, caplog, args):
        with pytest.raises(SystemExit):
            main(args)
        assert caplog.records[-1].levelno == 40

    @pytest.mark.parametrize(
        "boundary, location",
        [
            (["-l", "400"]),
            (["-r", "0"]),
            (["-u", "800"]),
            (["-b", "0"]),
        ],
    )
    def test_crop_errors(self, tmp_path, caplog, boundary, location):
        path = tmp_path / "file.png"
        im = Image.new("RGBA", (400, 800), "#0000ff")
        im.save(path)
        with pytest.raises(SystemExit):
            main(["crop", str(tmp_path), boundary, location])
        assert caplog.records[-1].levelno == 40


class TestNoErrors:
    @pytest.fixture(autouse=True)
    def input_dir(self, tmp_path):
        (tmp_path / "dir").mkdir()

    @pytest.fixture(autouse=True)
    def files(self, tmp_path, input_dir):
        im = Image.new("RGBA", (400, 400), "#0000ff")
        im.save(tmp_path / "bmp_file.bmp")
        im.save(tmp_path / "dir" / "dir_bmp_file.bmp")
        im.save(tmp_path / "gif_file.gif")
        im.save(tmp_path / "dir" / "dir_gif_file.gif")
        im.save(tmp_path / "png_file.png")
        im.save(tmp_path / "dir" / "dir_png_file.png")
        im.save(tmp_path / "ppm_file.ppm")
        im.save(tmp_path / "dir" / "dir_ppm_file.ppm")
        im = Image.new("RGB", (400, 400), "#0000ff")
        im.save(tmp_path / "jpg_file.jpg")
        im.save(tmp_path / "dir" / "dir_jpg_file.jpg")

    # Request ICOs for tests that do not involve images changing size.
    @pytest.fixture()
    def icos(self, tmp_path, input_dir):
        im = Image.new("RGBA", (400, 400), "#0000ff")
        im.save(tmp_path / "ico_file.ico")
        im.save(tmp_path / "dir" / "dir_ico_file.ico")

    def test_input_output_prepend_append_recursive(self, tmp_path, icos):
        output = (tmp_path / "output")
        assert output.exists() is False
        main([
            "crop",
            str(tmp_path), str(tmp_path / "dir"), str(tmp_path / "not_dir"),
            "-o", str(output),
            "-p", "prepend_",
            "-a", "_append",
            "--recursive",
        ])
        assert output.exists() is True
        output_files = list(output.rglob("*"))
        assert len(output_files) == 12
        ext_count = collections.Counter()
        for path in output_files:
            ext_count[str(path)[-3:]] += 1
        assert all(ext == 2 for ext in ext_count.values()) is True
        assert all(path.stem.startswith("prepend_") for path in output_files)
        assert all(path.stem.endswith("_append") for path in output_files)

    @pytest.mark.parametrize(
        "quality",
        [
            ("0"),
            ("100"),
        ],
    )
    def test_quality(self, tmp_path, caplog, icos, quality):
        output = (tmp_path / "output")
        main([
            "crop",
            str(tmp_path),
            "-o", str(output),
            "--quality", quality,
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 6
        count = caplog.messages.count(f"File saved with '{quality}' quality.")
        assert count == 6

    def test_force_no_overwrite(self, tmp_path):
        path = tmp_path / "jpg_file.jpg"
        assert Image.open(path).size == (400, 400)
        main([
            "crop",
            str(path),
            "-o", str(tmp_path),
            "-l", "300",
        ])
        assert Image.open(path).size == (400, 400)

    def test_force_overwrite(self, tmp_path):
        path = tmp_path / "jpg_file.jpg"
        assert Image.open(path).size == (400, 400)
        main([
            "crop",
            str(path),
            "-o", str(tmp_path),
            "-l", "300",
            "--force",
        ])
        assert Image.open(path).size == (100, 400)

    def test_crop(self, tmp_path):
        output = (tmp_path / "output")
        main([
            "crop",
            str(tmp_path),
            "-o", str(output),
            "-l", "-100",
            "-r", "500",
            "-u", "150",
            "-b", "200",
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
        for path in output_files:
            im = Image.open(path)
            assert im.size == (600, 50)

    def test_resize_width_height(self, tmp_path):
        output = (tmp_path / "output")
        main([
            "resize",
            str(tmp_path),
            "-o", str(output),
            "-W", "10",
            "-H", "250",
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
        for path in output_files:
            im = Image.open(path)
            assert im.size == (10, 250)

    def test_resize_ratio(self, tmp_path):
        output = (tmp_path / "output")
        main([
            "resize",
            str(tmp_path),
            "-o", str(output),
            "-W", "200",
            "-r",
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
        for path in output_files:
            im = Image.open(path)
            assert im.size == (200, 200)

    def test_resize_scale(self, tmp_path):
        output = (tmp_path / "output")
        factor = 2.5
        main([
            "resize",
            str(tmp_path),
            "-o", str(output),
            "-s", str(factor),
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
        for path in output_files:
            im = Image.open(path)
            assert im.size == (400 * factor, 400 * factor)

    @pytest.mark.parametrize(
        "_format",
        [
            ("BMP"),
            ("GIF"),
            ("ICO"),
            ("JPEG"),
            ("PNG"),
            ("PPM"),
        ],
    )
    def test_convert(self, tmp_path, icos, _format):
        output = (tmp_path / "output")
        main([
            "convert",
            str(tmp_path),
            "-o", str(output),
            "--format", _format
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 6
        for file in output_files:
            assert Image.open(file).format == _format
        for path in output_files:
            if _format == "JPEG":
                assert path.suffix[1:] == "jpg"
            else:
                assert path.suffix[1:] == _format.lower()

    def test_rotate(self, tmp_path):
        output = (tmp_path / "output")
        main([
            "rotate",
            str(tmp_path),
            "-o", str(output),
            "-A", "-45",
            "--fill",
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
        for file in output_files:
            im = Image.open(file)
            assert im.width == 566
            assert im.height == 566
            if im.mode == "RGBA":
                assert im.getpixel((0, 0)) == (0, 0, 0, 255)
            elif im.mode == "P":
                assert im.getpixel((0, 0)) == 0
            else:
                assert im.getpixel((0, 0)) == (0, 0, 0)

    def test_transpose(self, tmp_path):
        output = (tmp_path / "output")
        main([
            "transpose",
            str(tmp_path),
            "-o", str(output),
        ])
        output_files = list(output.rglob("*"))
        assert len(output_files) == 5
