# How to contribute

## Reporting bugs

Bug reports are very much appreciated! If you encounter a bug, don't hesitate
to [open a new issue](https://gitlab.com/emerac/pyimgutil/-/issues).

In the interest of fixing bugs as soon as possible, try to include in your
report:

* What you were trying to do when the bug occurred
* An explanation of the unexpected outcome (bug)
* How to reproduce the bug

## Requesting a feature

At the moment, the best way to request a feature is to
[open a new issue](https://gitlab.com/emerac/pyimgutil/-/issues).

## Writing code

First of all, thank you! It takes time and energy to add to a codebase, so it
greatly appreciated that you would spend yours helping out here.

You can submit your contributions as a pull request that will be reviewed.
Here are a few guidelines concerning pull requests:

* Try to keep your changes small and focused.
* Test your code against the preexisting tests.
* If the current tests do not cover your code, please add new ones
that do.

---
**Thank you!**
