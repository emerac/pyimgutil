# Compatibility

## Python

This program requires Python to be installed. To determine if you have Python
installed, open a terminal window and enter the command: `python --version`.
If it is installed, the command will output "Python" followed by a version
number. You can check that version number against the table below.

| Version | Compatibility | Tested      |
| ------- | ------------- | ----------- |
| 3.9     | Full          | Yes         |
| 3.8     | Full          | Yes         |
| 3.7     | Full          | Yes         |

## Operating systems

| OS      | Compatibility | Tested | Notes                                    |
| ------- | ------------- | ------ | ---------------------------------------- |
| Linux   | Full          | Yes    | None                                     |
| Windows | Unknown       | No     | Although this program was not tested on this platform, no features used in this program are known to be incompatible with this platform. |
| macOS   | Unknown       | No     | Although this program was not tested on this platform, no features used in this program are known to be incompatible with this platform. |
