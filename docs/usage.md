# Usage

[[_TOC_]]

## Overview

Although this program is thoroughly tested, it is impossible to say that no
bugs can exist. Therefore, **it is highly recommended that you back up your
data before running commands**, so a bug or mistyped command does not
potentially result in loss of data.

Supported image formats:

* BMP
* GIF
* ICO
* JPEG
* PNG
* PPM

Running the command `pyimgutil -h` will help get you started, but read on if
you would like more detailed information with example commands.

## Convert

Change the format of an image.

For convert command help on the command-line, run `pyimgutil convert -h`.

### Available arguments

Converting between supported formats is done using the `--format` argument,
which is common to all commands. The primary purpose of the convert command is
to make the `--format` argument available without doing any other image
processing (e.g. crop, resize, etc.).

In addition to the `--format` argument mentioned above, a number of other
[command-non-specific arguments](#arguments-common-to-all-commands) are also
available.

### Command-line syntax

At least one [input](#input-required) must be specified directly following
the convert command. Any optional arguments may be specified following the
input argument(s).

[Click here](#format-optional-format) for more information about using the
`--format` argument.

Example command:

`pyimgutil convert path/to/input/ --format PNG`

### Detailed example

Converting between formats can have unintended side-effects depending on the
formats involved. Consider the example command below:

`pyimgutil convert path/to/file.png --format JPEG`

The PNG file (mode: RGBA) will be converted to a JPEG file (mode: RGB). As a
consequence of this, any transparent pixels in the PNG file will be converted
to the default JPEG pixel (black).

## Crop

Remove unwanted outer areas from an image.

For crop command help on the command-line, run `pyimgutil crop -h`.

### Available arguments

There are four crop-specific arguments available:

* `--left`/`-l` - the left crop boundary, in pixels
* `--right`/`-r` - the right crop boundary, in pixels
* `--upper`/`-u` - the upper crop boundary, in pixels
* `--bottom`/`-b` -the bottom crop boundary, in pixels

The four arguments above specify the crop boundaries. The area bounded by
these arguments will remain after the crop.

In addition to the command-specific arguments mentioned above, a number of
[command-non-specific arguments](#arguments-common-to-all-commands) are also
available.

### Command-line syntax

At least one [input](#input-required) must be specified directly following
the crop command. Any optional arguments may be specified following the input
argument(s).

The four crop-specific arguments may be used in any combination. Consider the
following example commands:

`pyimgutil crop path/to/input/ --left 20 --right 100 --upper -100 --bottom 40`

`pyimgutil crop path/to/input/ --left 200 --right 1000`

`pyimgutil crop path/to/input/ --upper -10 --bottom 80`

`pyimgutil crop path/to/input/ --right 281`

Each of the four crop-specific arguments is followed by a whole number. This
number represents the pixel location on the image to mark as a crop boundary.
All locations are referenced from the top-left corner of the image (e.g.
`--left 10 --right 500` refers to locations 10 pixels and 500 pixels away from
the left edge of the image, respectively. `--upper -100 --bottom 200` refers
to locations 100 pixels and 200 pixels away from the upper edge of the image,
respectively).

If a negative value or a value outside of the image's dimensions is given, a
number of pixels (black or transparent depending on the image's mode) will be
added to the image.

An unspecified crop boundary will result in no crop being performed on that
side of the image.

### Detailed example

Consider a case where the input file, `file.jpg`, is an image that is 800
pixels wide and 400 pixels tall. Both of the example commands below perform
the same action:

`pyimgutil crop path/to/file.jpg --left 100 --right 400 --upper -200`

`pyimgutil crop path/to/file.jpg -l 100 -r 400 -u -200 `

* `-l 100` crops out everything to the left of 100 pixels from the left edge
* `-r 400` crops out everything to the right of 400 pixels from the left edge
* `-u -200` adds 200 pixels above the image
* Because the bottom argument was not specified, no crop is performed on the
bottom side of the image

The resultant image will be 300 pixels wide (the numbers of pixels between the
left and right arguments) and 600 pixels tall (the numbers of pixels between
the upper argument and the bottom edge of the image).

## Resize

Resize an image.

For resize command help on the command-line, run `pyimgutil resize -h`.

### Available arguments

There are four resize-specific arguments available:

* `--width` - the pixel width to which the image shall be resized
* `--height` - the pixel height to which the image shall be resized
* `--ratio`/`-r` - maintain the image's original aspect ratio
* `--scale`/`-s` - proportionally scale the image's dimensions by a factor
    * The factor is relative to the original image. A factor equal to `1`
    would result in an unchanged image, greater than `1` in a larger image,
    and less than `1` in a smaller image.

In addition to the command-specific arguments mentioned above, a number of
[command-non-specific arguments](#arguments-common-to-all-commands) are also
available.

### Command-line syntax

At least one [input](#input-required) must be specified directly following
the resize command. Any optional arguments may be specified following the
input argument(s).

Some of the resize-specific commands mentioned above may not be used
together. For example:

* If both `--width` and `--height` are used, `--ratio` cannot be used because
the aspect ratio must change if both the width and height do not change
proportionally. However, `--ratio` can be used with either `--width` or
`--height`.
* If either `--width` or `--height` are used, `--scale` cannot be used
because `--scale` changes both dimensions proportionally.

Both `--width` and `--height` are followed by a whole number, while scale
can be followed by a whole number or a decimal number.

Consider the following example commands:

`pyimgutil resize path/to/input/ --width 500`

`pyimgutil resize path/to/input/ --width 500 --height 100`

`pyimgutil resize path/to/input/ --width 500 --ratio`

`pyimgutil resize path/to/input/ --scale 2`

`pyimgutil resize path/to/input/ -s 0.83`

### Detailed examples

`pyimgutil resize path/to/input/ --width 1000 --height 400`

All processed images will be 1000 pixels wide and 400 pixels tall.

`pyimgutil resize path/to/input/ --height 1000 -r`

All processed images will be 1000 pixels tall and the width of each image will
be adjusted so the aspect ratio does not change.

`pyimgutil resize path/to/input/ --scale 2.4`

All processed images will have dimensions 240% of their original dimensions.

`pyimgutil resize path/to/input/ -s 0.51`

All processed images will have dimensions 51% of their original dimensions.

## Rotate

Reorient an image.

For rotate command help on the command-line, run `pyimgutil rotate -h`.

### Available arguments

There are two rotate-specific arguments available:

* `--angle`/`-A` - the number of degrees counter clockwise by which to rotate
* `--fill` - fill open areas with black pixels

In addition to the command-specific arguments mentioned above, a number of
[command-non-specific arguments](#arguments-common-to-all-commands) are also
available.

### Command-line syntax

At least one [input](#input-required) must be specified directly following
the rotate command. Any optional arguments may be specified following the
input argument(s).

Any whole number can be given following the `--angle` argument. If a negative
number is given, the image will be rotated clockwise.

Example commands:

`pyimgutil rotate path/to/input/ --angle 270`

`pyimgutil rotate path/to/input/ -A -45 --fill`

### Detailed example

Consider a case where the input file, `file.png`, is an image that is 400
pixels wide and 800 pixels tall. The example command below will rotate the
image 45 degrees counter clockwise:

`pyimgutil rotate path/to/file.png -A 45`

Rotating the original image would cause the contents of the image to overflow
its container, so the dimensions of the output image have changed in order to
accommodate all of the original image.

There are also transparent areas in the spaces between the now rotated image
and its container. This is a feature of RGBA images. If transparency is not
desired, however, it can be filled with black pixels by using the `--fill`
argument:

`pyimgutil rotate path/to/file.png -A 45 --fill`

## Transpose

Flip an image horizontally.

For transpose command help on the command-line, run `pyimgutil transpose -h`.

### Available arguments

There are no command-specific arguments. Any images included as input will be
transposed.

A number of [command-non-specific arguments](#arguments-common-to-all-commands)
are available for use.

### Command-line syntax

At least one [input](#input-required) must be specified directly following
the transpose command. Any optional arguments may be specified following the
input argument(s).

Example command:

`pyimgutil transpose path/to/input/`

### Detailed examples

`pyimgutil transpose path/to/input/`

In the command above, each processed image will be flipped about a vertical
axis running down the center of the image.

## Arguments common to all commands

### Input (required)

The path to the file(s) and/or directories to process.

Any number of files or directories can be specified, but at least one
input is required following any image processing command.

Note that if a directory is specified, any files in that directory, which are
of a supported image format, will be processed.

Example commands:

`pyimgutil crop path/to/file.jpg -l 100`

`pyimgutil crop path/to/dir/ -l 100`

`pyimgutil crop path/to/file.jpg path/to/dir/ -l 100`

### Output (optional) [`--output`/`-o`]

The location in which to place processed image files.

By default, processed images are placed in the current working directory. A
different directory can be chosen by using the `--output` argument followed by
a path. If the last directory in the specified path does not exist, it will be
created.

Example command:

`pyimgutil resize path/to/input/ -s 2.0 -o path/to/output/`

In the command above, the `output` directory will be created if it does not
exist.

### Prepend (optional) [`--prepend`/`-p`]

Add text to the beginning of each output filename.

By default, processed image files retain their original names. However,
processed image filenames can be prepended with some text by adding
`--prepend` followed by some text.

Example commands:

`pyimgutil crop path/to/file.jpg -b 150 --prepend cropped_`

`pyimgutil crop path/to/file.jpg -b 150 -p 2021_01_01_`

The above commands would result in `file.jpg` being processed and then
placed in the current working directory under the name `cropped_file.jpg`
and `2021_01_01_file.jpg`, respectively.

### Append (optional) [`--append`/`-a`]

Add text to the end of each output filename.

By default, processed image files retain their original names. However,
processed image filenames can be appended with some text by adding
`--append` followed by some text.

Example commands:

`pyimgutil resize path/to/file.jpg --width 1000 --append _resized`

`pyimgutil resize path/to/file.jpg --width 1000 -a _2021_01_01`

The above commands would result in `file.jpg` being processed and then
placed in the current working directory under the name `file_cropped.jpg`
and `file_2021_01_01.jpg`, respectively.

### Quality (optional) [`--quality`]

Set the image quality (0-100) to use when saving JPEGs.

JPEG is a lossy format, which means that, when compared to a lossless format,
file sizes are smaller at the cost of some amount of visual quality. Usually,
a favorable balance between file size and visual quality can be found by
adjusting the quality level with which the file is saved. Use the `--quality`
argument followed by a number between 0 (lowest quality) and 100 (highest
quality) to adjust this level. Note that there are diminishing returns in
visual quality for levels above 95.

When the quality argument has not been specified, the program always attempts
to save an image with its original quality level. Sometimes this is not
possible because the information necessary to reproduce the compression is not
available. As a result, if, for example, an image is saved without doing any
processing, a small increase in file size may be noticed due to differences
in the compression used.

Example command:

`pyimgutil resize path/to/input/ -s 1.5 --quality 95`

### Format (optional) [`--format`]

Choose the image format to output.

By specifying `--format` followed by one of the six supported formats listed
at the top, all processed files will be returned in the chosen format.

Example commands:

`pyimgutil crop path/to/input/ -l 100 -r 800 --format PNG`

### Recursive (optional) [`--recursive`]

Search input directories recursively for supported image files.

By default, input directories are only searched one level down. By specifying
`--recursive`, input directories and any and all directories contained in
those input directories are also searched.

Example command:

`pyimgutil resize path/to/input/ -W 500 -H 1000 --recursive`

### Force (optional) [`--force`/`f`]

Allow processed image files to overwrite conflicting files in the output
directory.

Any time a processed image file is being saved, a check is performed to make
sure that the processed file will not overwrite an existing file in the
output location. By default, if a conflict is found, the program skips the
conflict and displays an error message. This protection can be overridden,
though, by adding the `--force` argument.

Example command:

`pyimgutil crop path/to/file.jpg -l 500 -r 700 --force`

### Verbose (optional) [`--verbose`/`-v`]

Provide log output. This argument can be repeated.

By default, should an error occur, the only messages displayed would be the
error messages. However, more description about what the program is doing is
available by adding the `--verbose`/`-v` argument. This argument can be
specified twice (`-vv`) for extra verbosity.

Example commands:

`pyimgutil crop path/to/input/ --verbose -u 100`

`pyimgutil crop path/to/input/ -vv -u 100`
