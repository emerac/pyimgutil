# Installation

Installation is quick and easy thanks to the magic of pip!

If you have Python installed, then pip is most likely installed as well. You
can check it, though, by running: `pip --version`. If it is installed, the
command will output "pip" followed by a version number.

To install this program, run: `pip install pyimgutil`.

To make sure everything was installed properly, run the command
`pyimgutil -h`. A help message should be displayed.
