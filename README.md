# Python Image Utility (pyimgutil)

## About

This is a simple command-line utility, written in Python, for individual or
batch image processing.

There are a number of excellent programs out there that already do what this
program does, but sometimes a quick, lightweight, solution is nice to have
handy, too.

Features:

* convert
* crop
* resize
* rotate
* transpose
* change quality

If you find this program useful and there is a feature you would like to
suggest, see
[CONTRIBUTING](https://gitlab.com/emerac/pyimgutil/-/blob/master/CONTRIBUTING.md).

## How to Get Started

### Compatibility

If you're running Python 3.7+ on any operating system, you're most likely
good-to-go.

For more details, view the
[compatiblity docs](https://gitlab.com/emerac/pyimgutil/-/blob/master/docs/compatibility.md).

### Installation

This program is available through pip. Running `pip install pyimgutil` will
install it.

For more details, view the
[installation docs](https://gitlab.com/emerac/pyimgutil/-/blob/master/docs/installation.md).

### Usage

After installing, `pyimgutil -h` will help you get started.

For command explanations and examples, view the
[usage docs](https://gitlab.com/emerac/pyimgutil/-/blob/master/docs/usage.md).

## Contributing

Please report any bugs to the
[GitLab issue tracker](https://gitlab.com/emerac/pyimgutil/-/issues).
See
[CONTRIBUTING](https://gitlab.com/emerac/pyimgutil/-/blob/master/CONTRIBUTING.md)
for more details.

## Credits

Big thanks to the Pillow team for providing such a powerful and user-friendly
image processing library.

## License

This program is free software and is licensed under the GNU General
Public License. For the full license text, view the
[LICENSE](https://gitlab.com/emerac/pyimgutil/-/blob/master/LICENSE) file.

Copyright © 2021 emerac
